name := "aWebAppBackend"
 
version := "1.0" 
      
lazy val `awebappbackend` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  jdbc,
  ehcache,
  ws,
  specs2 % Test,
  guice,
  "com.jason-goodwin" % "authentikat-jwt_2.12" % "0.4.5",
  "org.scalaz" %% "scalaz-core" % "7.2.10",
  "com.typesafe.play" % "play-json-joda_2.12" % "2.6.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % "test",
  "org.mockito" % "mockito-core" % "1.9.5" % "test"
)

      