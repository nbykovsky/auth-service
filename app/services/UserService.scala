package services

import javax.inject.Inject
import models.user.{User, UserDao, UserId, UserR, UserUpd}
import scalaz.\/
import utils.actionUtils

import scala.concurrent.{ExecutionContext, Future}

class UserService @Inject()(userDAO: UserDao) {

  def createUserService(user: UserR)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit] = userDAO.create(user)

  def deleteUserService(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit] = userDAO.deleteById(userId)

  def findUserByIdService(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ User] = userDAO.findById(userId)

  def getAllUserService(implicit ec: ExecutionContext): Future[actionUtils.Error \/ List[User]] = userDAO.getAllUsers

  def updateUserService(userId: UserId, userUpd: UserUpd)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit] = userDAO.update(userId, userUpd)
}
