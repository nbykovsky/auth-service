package services

import javax.inject.{Inject, Singleton}
import models.session.{SessionDao, SessionExt, SessionInt, SessionR, CommonAo}
import models.user.{User, UserDao}
import org.joda.time.DateTime
import scalaz.{EitherT, \/}
import utils.{actionUtils, jwtUtils}
import utils.common._
import utils.jwtUtils.{createToken, decodePayload, isValidToken}
import scalaz.Scalaz._
import scalaz._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthService @Inject()(userDAO: UserDao, sessionDAO: SessionDao, commonAo: CommonAo){



  /**
    * Method takes credential and generates jwt token
    */
  def loginService(sessionR: SessionR)(implicit ec: ExecutionContext): Future[actionUtils.Error\/SessionExt] =
    userDAO.findById(sessionR.userId) map {either =>
      either.filterOrElse(user => user.password == sessionR.password, actionUtils.UnauthorizedError("Password is wrong"))
        .map(user => createToken(SessionInt(user.userId, commonAo.getCurrentTime.plusMinutes(commonAo.EXP_INTERVAL), user.permissions), commonAo.SECRET_KEY))
        .map(SessionExt)
    }

  /**
    * Makes jwt token non valid by putting it into "blacklist"
    */
  def logoutService(ssnExt: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] = sessionDAO.deleteSession(ssnExt)



  /**
   * Takes one valid token and returns another (with updated expitation date)
   */
  def renewService(ssnExtOld: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/SessionExt] = {
    val ns = jwtUtils.decodePayload(ssnExtOld, commonAo.SECRET_KEY).map(_.copy(exp = commonAo.getCurrentTime.plusMinutes(commonAo.EXP_INTERVAL)))
      .map(t=>jwtUtils.createToken(t, commonAo.SECRET_KEY)).map(SessionExt)
    val newSession= for {
      ssn <- ns                                      :EitherT[Future, actionUtils.Error, SessionExt]
      _   <- sessionDAO.deleteSession(ssnExtOld)     :EitherT[Future, actionUtils.Error, Unit]
    } yield ssn
    newSession.run
  }


  /**
   * check session (black list + token + expiration)
   */
  def checkSession(ssnExt: SessionExt)(implicit ec: ExecutionContext): Future[ actionUtils.Error\/Unit] = {
    val result: EitherT[Future, actionUtils.Error, Unit] = for {
      _ <- jwtUtils.isValidToken(ssnExt.token, commonAo.SECRET_KEY)         :EitherT[Future, actionUtils.Error, Unit]
      _ <- sessionDAO.checkSessionInBlackList(ssnExt)                       :EitherT[Future, actionUtils.Error, Unit]
      ssnInt <- jwtUtils.decodePayload(ssnExt, commonAo.SECRET_KEY)         :EitherT[Future, actionUtils.Error, SessionInt]
      _ <- Right[actionUtils.Error, Boolean](ssnInt.exp.isAfter(commonAo.getCurrentTime)).filterOrElse(x=>x, actionUtils.UnauthorizedError("Token expired"))
    } yield ()
    result.run
  }
}