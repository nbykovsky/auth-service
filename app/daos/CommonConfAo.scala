package daos

import models.session.CommonAo
import org.joda.time.DateTime

class  CommonConfAo extends CommonAo{
  override def getCurrentTime: DateTime = DateTime.now()

  /**
    * How long token will be valid (Minutes)
    */
  override val EXP_INTERVAL: Int = 2
  override val SECRET_KEY: String = "Secret Key"
}
