package daos.inMemory

import javax.inject.Singleton
import models.session.{SessionDao, SessionExt}
import scalaz.{-\/, \/, \/-}
import utils.actionUtils

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import utils.common._

@Singleton
class SessionMemDao extends SessionDao{

  val sessionBlackList: mutable.Set[SessionExt] = mutable.Set[SessionExt]()

  override def deleteSession(session: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] = {
    if (sessionBlackList.contains(session)) Future.successful(-\/(actionUtils.UnauthorizedError("Session is not valid")))
    else {
      sessionBlackList+=session
      Future.successful(\/-(()))
    }
  }

  override def checkSessionInBlackList(session: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] =
    Future.successful(Right(sessionBlackList.contains(session)).filterOrElse(x => !x,actionUtils.UnauthorizedError("Token was invoked")).map(_ => ()))

}
