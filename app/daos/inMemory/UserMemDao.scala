package daos.inMemory

import javax.inject.Singleton
import models.user.{User, UserDao, UserId, UserR, UserUpd}
import scalaz._
import utils.actionUtils

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import utils.common._

@Singleton
class UserMemDao extends UserDao {

    val userRepository: mutable.Map[UserId, User] = mutable.Map("email"->User("user", "password", "email", "email", Set()))

    override def findById(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error\/User] = Future.successful {
      userRepository.get(userId) toRight actionUtils.NotFoundError("User doesn't exists")
    }


    override def create(user: UserR)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] = Future.successful {
        userRepository.get(user.email).map(_ => actionUtils.ConflictError("User exists")) toLeft {
          userRepository += (user.email -> User(user.name, user.password, user.email, user.email, Set()))
          ()
        }
    }


    override def deleteById(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] =
      findById(userId) map { either =>
        either map {_ =>
          userRepository -= userId
        }
      }


    override def update(userId: UserId, userUpd: UserUpd)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit] = {
      println(userRepository)
      val result =
      findById(userId) map { either =>
        either.map(user =>
          User(
            userUpd.name.getOrElse(user.name),
            userUpd.password.getOrElse(user.password),
            userUpd.email.getOrElse(user.email),
            user.userId,
            userUpd.permissions.getOrElse(user.permissions))) map (newUser => userRepository(newUser.userId) = newUser)
      }
      println(userRepository)
      result
    }

  override def getAllUsers(implicit ec: ExecutionContext): Future[actionUtils.Error\/List[User]] = Future.successful(\/-(userRepository.toList.map(_._2)))
}

