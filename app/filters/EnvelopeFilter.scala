package filters


import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import akka.stream.Materializer
import akka.util.ByteString
import com.fasterxml.jackson.core.JsonParseException
import com.google.common.base.Utf8
import javax.inject.Inject
import play.api.http.HttpEntity.Strict
import play.api.libs.json._
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try


/*
 * Filter formatting output,
 * if response code < 300 it makes {"ok": <original body>}
 * otherwise it makes {"error": <original body>}
 * also it adds contentType="application/json" header
 */
class EnvelopeFilter @Inject()(implicit val mat: Materializer,ec: ExecutionContext) extends Filter {

  override def apply(nextFilter: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] =
    nextFilter(rh).flatMap { result =>
      val word = if (result.header.status < 300) "ok" else "error"
      val maybeNewBody = result.body.consumeData.map(x => Json.obj(word -> {
        Try(Json.parse(x.utf8String)).toOption match {
          case Some(y) => y match {
            case JsObject(c) => c
            case JsArray(c) => c
            case JsNumber(c) => c
            case JsString(c) => c
            case _ => x.utf8String
          }
          case None => x.utf8String
        }
      })).map(x => ByteString(x.toString()))
      maybeNewBody.map(x => result.copy(body = Strict(x, Some("application/json"))))
    }
}