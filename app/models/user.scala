package models

import play.api.libs.json.{Json, OWrites, Reads}
import scalaz.\/
import utils.actionUtils

import scala.concurrent.{ExecutionContext, Future}

object user {

  type UserId = String
  type Permission = String

  implicit val userRequestReads: Reads[UserR] = Json.reads[UserR]
  implicit val userReads: Reads[User] = Json.reads[User]
  implicit val userWrites: OWrites[User] = Json.writes[User]
  implicit val userUpdateReads: Reads[UserUpd] = Json.reads[UserUpd]

  case class UserR(name: String, password: String, email: String)

  case class User(name: String, password: String, email: String, userId: UserId, permissions: Set[Permission])

  case class UserUpd(name: Option[String], password: Option[String], email: Option[String], permissions: Option[Set[Permission]])


  trait UserDao {

    def findById(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ User]

    def getAllUsers(implicit ec: ExecutionContext): Future[actionUtils.Error \/ List[User]]

    def create(user: UserR)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit]

    def deleteById(userId: UserId)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit]

    def update(userId: UserId, userUpd: UserUpd)(implicit ec: ExecutionContext): Future[actionUtils.Error \/ Unit]
  }

}