package models

import models.user.{Permission, UserId}
import org.joda.time.DateTime
import play.api.libs.json.{Json, OWrites, Reads}
import scalaz.\/
import utils.actionUtils
import play.api.libs.json.JodaReads._
import play.api.libs.json.JodaWrites._

import scala.concurrent.{ExecutionContext, Future}

object session {

  type TokenJWT = String

  implicit val sessionWrites: OWrites[SessionExt] = Json.writes[SessionExt]
  implicit val sessionIntReads: Reads[SessionInt] = Json.reads[SessionInt]
  implicit val sessionIntWrites: OWrites[SessionInt] = Json.writes[SessionInt]
  implicit val sessionRequestReads: Reads[SessionR] = Json.reads[SessionR]

  case class SessionExt(token: TokenJWT)

  case class SessionInt(uid: UserId, exp: DateTime, prm: Set[Permission])

  case class SessionR(userId: UserId, password: String)

  trait SessionDao {

    /*
     * puts session into black list
     */
    def deleteSession(session: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit]

    /*
     * check whether session is in a black list
     */
    def checkSessionInBlackList(session: SessionExt)(implicit ec: ExecutionContext): Future[actionUtils.Error\/Unit]


  }

  /**
    * move time to separate trait for mocking during unittests
    */
  trait CommonAo {
    def getCurrentTime: DateTime

    /**
      * How long token will be valid (Minutes)
      */
    val EXP_INTERVAL: Int

    val SECRET_KEY: String
  }

}
