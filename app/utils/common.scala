package utils

import scalaz.{EitherT, Monad, \/}

import scala.concurrent.Future

object common {
  implicit def either2valid[A, B](e: Either[A, B]): A\/B = \/.fromEither(e)

  implicit def valid2either[A, B](v: A\/B): Either[A,B] = v.toEither

  implicit def any2future[A](a: A): Future[A] = Future.successful(a)

  implicit def futureValid2eitherT[A, B](a: Future[A\/B]): EitherT[Future, A, B] = EitherT[Future, A, B](a)

  implicit def valid2eitherT[A, B](a:A\/B): EitherT[Future, A, B] = EitherT[Future, A, B](Future.successful(a))

  implicit def either2eitherT[A, B](a: Either[A, B]): EitherT[Future, A, B] = EitherT[Future, A, B](Future.successful(\/.fromEither(a)))


}
