package utils

import authentikat.jwt.{JsonWebToken, JwtClaimsSet, JwtHeader}
import models.session.{SessionExt, SessionInt, TokenJWT}
import play.api.libs.json.Json
import scalaz.{-\/, \/, \/-}
import scalaz._
import utils.common._

object jwtUtils {

//  val jwtSecretKey = "secret key"
  val jwtSecretAlgo = "HS256"

  def createToken(intSession: SessionInt, jwtSecretKey: String): TokenJWT = {
    val json = Json.toJson(intSession)
    val header = JwtHeader(jwtSecretAlgo)
    val claimsSet = JwtClaimsSet(json.toString())
    JsonWebToken(header, claimsSet, jwtSecretKey)
  }

  def isValidToken(jwtToken: TokenJWT, jwtSecretKey: String): actionUtils.Error\/Unit =
    Option(JsonWebToken.validate(jwtToken, jwtSecretKey)).filter(x=>x).toRight(actionUtils.UnauthorizedError("Token is not valid")).map(_ => ())


  def decodePayload(ssnExt: SessionExt, jwtSecretKey: String): actionUtils.Error\/SessionInt =
    ssnExt.token match {
      case JsonWebToken(header, claimsSet, signature) =>
        // theoretically we could get an exception here if json has wrong format
        // since we generated this json (it was signed), we could assume that format is correct
        \/-(Json.parse(claimsSet.asJsonString).as[SessionInt])
      case x                                          =>
        -\/(actionUtils.BadRequestError("This shouldn't happen"))
    }

}

