package utils

import javax.inject.Inject
import models.session.{SessionExt, SessionInt, CommonAo}
import play.api.libs.json.{JsError, Reads}
import play.api.mvc._
import scalaz.{-\/, EitherT, \/-}
import scalaz.Scalaz._
import services.AuthService

import scala.concurrent.{ExecutionContext, Future}
import utils.common._

object actionUtils {



  case class RequestWithAuth[A](ssnInt: SessionInt, request: Request[A]) extends WrappedRequest(request)

  class JwtAuth @Inject() (val parser: BodyParsers.Default) (implicit val executionContext: ExecutionContext, cc: ControllerComponents, as: AuthService, commonAo: CommonAo)
    extends AbstractController(cc) with ActionBuilder[RequestWithAuth, AnyContent]  {
    override def invokeBlock[A](request: Request[A], block: RequestWithAuth[A] => Future[Result]): Future[Result] = {

      val ssnIntWrapped: EitherT[Future, actionUtils.Error, SessionInt] = for {
        ssnExt <- request.headers.get("Authorization")
          .toRight(UnauthorizedError("Token doesn't exists"))
          .map(SessionExt)                                              :EitherT[Future, actionUtils.Error, SessionExt]
          _ <- as.checkSession(ssnExt)                                  :EitherT[Future, actionUtils.Error, Unit]
          ssnInt <- jwtUtils.decodePayload(ssnExt, commonAo.SECRET_KEY)   :EitherT[Future, actionUtils.Error, SessionInt]
        } yield ssnInt
      ssnIntWrapped.run flatMap  {
        case -\/(err) => Future(dispatchError(err))
        case \/-(ssnInt) => block(RequestWithAuth(ssnInt, request))
      }
    }
// EitherT[Future, ActionUtils.Error, Unit](Future.successful(-\/(NotFoundError("test"))))

    def dispatchError(error: Error): Result = {
      error match {
        case BadRequestError(msg) => BadRequest(msg)
        case UnauthorizedError(msg) => Unauthorized(msg)
        case NotFoundError(msg) => NotFound(msg)
        case ConflictError(msg) => Conflict(msg)
        case ForbiddenError(msg) => Forbidden(msg)
      }
    }

    def validateJson[A : Reads]: BodyParser[A] = parse.json.validate(
      _.validate[A].asEither.left.map(e => BadRequest(JsError.toJson(e)))
    )

  }

  sealed abstract class Error(msg: String)
  case class BadRequestError(msg: String) extends Error(msg)
  case class UnauthorizedError(msg: String) extends Error(msg)
  case class NotFoundError(msg: String) extends Error(msg)
  case class ConflictError(msg: String) extends Error(msg)
  case class ForbiddenError(msg: String) extends Error(msg)


}
