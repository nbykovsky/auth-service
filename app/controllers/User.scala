package controllers

import javax.inject._
import play.api.mvc._
import play.api.libs.json._
import scalaz.{-\/, \/-}
import services.UserService
import utils.actionUtils.JwtAuth

import scala.concurrent.ExecutionContext
import utils.common._
import scalaz.{Inject => Inj, _}
import Scalaz._
import models.user.{ UserId, UserR, UserUpd}
import models.session._
import models.user._


@Singleton
class User @Inject()(implicit ec: ExecutionContext, cc: ControllerComponents, userService: UserService, jwtAction: JwtAuth) extends AbstractController(cc)  {

  /**
   * Create new user
   */
  def create: Action[UserR] = Action(jwtAction.validateJson[UserR]).async { request =>
    val user = request.body
    userService.createUserService(user) map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(_) => Created("user created")
    }
  }

  /**
   * Delete User
   */
  def delete(userId: UserId): Action[AnyContent] = jwtAction.async {
    userService.deleteUserService(userId) map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(_) => Ok("User deleted")
    }
  }

  /**
   * Update User's data
   */
  def updateUser(userId: UserId): Action[UserUpd] = jwtAction(jwtAction.validateJson[UserUpd]).async { request =>
    userService.updateUserService(userId, request.body) map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(_) => Ok("User updated")
    }
  }

  /**
   * Get list of users
   */
  def getUsersList: Action[AnyContent] = jwtAction.async { request =>
    userService.getAllUserService map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(users) => Ok(Json.toJson(users))
    }
  }

  /**
   * Get one user
   */
  def getUser(userId: UserId): Action[AnyContent] = jwtAction.async { _ =>
    userService.findUserByIdService(userId) map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(user) => Ok(Json.toJson(user))
    }
  }


}
