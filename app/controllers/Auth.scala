package controllers

import javax.inject._
import play.api.mvc._
import play.api.libs.json._
import scalaz.{-\/, EitherT, \/, \/-}
import services.AuthService
import utils.actionUtils
import utils.actionUtils.JwtAuth

import scala.concurrent.{ExecutionContext, Future}
import utils.common._
import scalaz.{Inject => Inj, _}
import Scalaz._
import models.session.{SessionExt, SessionR}
import models.session._
import models.user._


@Singleton
class Auth @Inject()(implicit ec: ExecutionContext, cc: ControllerComponents,as: AuthService, jwtAction: JwtAuth) extends AbstractController(cc)  {


  /**
   * Send login/password and receive token
   */
  def login: Action[SessionR] = Action(jwtAction.validateJson[SessionR]).async { request =>
    as.loginService(request.body) map {
      case -\/(err) => jwtAction.dispatchError(err)
      case \/-(session) => Ok(Json.toJson(session.token))
    }
  }

  /**
   * Invoke token
   */
  def logout: Action[AnyContent] = jwtAction.async{ request =>
    request.headers.get("Authorization") match {
      case Some(h) =>
        as.logoutService(SessionExt(h)) map {
          case -\/(err) =>jwtAction.dispatchError(err)
          case \/-(_) => Ok("User was logged out")
        }
      case None => jwtAction.dispatchError(actionUtils.ForbiddenError("Header not found"))
    }
  }

  /**
   * Replace one valid token to another
   */
  def renew: Action[AnyContent] = jwtAction.async { request =>
    request.headers.get("Authorization") match {
      case Some(h) =>
        as.renewService(SessionExt(h)) map {
          case -\/(err) => jwtAction.dispatchError(err)
          case \/-(ssnExt) => Ok(Json.toJson(ssnExt.token))
        }
      case None => jwtAction.dispatchError(actionUtils.ForbiddenError("Header not found"))
    }

  }

  /**
   * Checks whether token is valid
   */
  def validateToken = jwtAction(_ => Ok("Token is valid"))


  /**
   * Recover password
   */
  def recoverPassword = ???

  /**
   * Confirm (endpoint will be used for email confirmation )
   */
  def confirmAction = ???

}
