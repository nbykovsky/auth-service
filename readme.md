## Simple authorisation service

### Non protected endpoints
* [Login](docs/login.md): `POST /login`
* [Create user](docs/create.md): `POST /user`

### Protected endpoints
* [Validate jwt](docs/validate.md): `GET /check`
* [Renew jwt](docs/renew.md): `GET /renew`
* [Logout](docs/logout.md): `DELETE /logout`
* [Update user](docs/updateUser.md): `PUT /user/<userId>`
* [Get user](docs/getUser.md): `GET /user/<userId>`
* [All users](docs/getAllUsers.md) `GET /user`