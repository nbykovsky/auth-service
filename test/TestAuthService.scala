import java.util.concurrent.Executors

import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import akka.stream.Materializer
import daos.inMemory.{SessionMemDao, UserMemDao}
import models.session
import models.session.{CommonAo, SessionDao, SessionExt, SessionR}
import models.user.{User, UserDao, UserR}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatestplus.play.{PlaySpec, _}
import services.AuthService

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.scalatest._
import org.mockito.Matchers._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.specs2.mock.Mockito
import play.api.mvc.{Action, EssentialAction}
import play.api.test.FakeRequest
import play.api.libs.json.{JsError, Reads}
import play.libs.Json
import scalaz.\/-
import utils.actionUtils.JwtAuth

class TestAuthService extends PlaySpec with MockitoSugar {

  implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1))


  "A loginService" must {
    "return valid session object" in {
      val mockCommonAo = mock[CommonAo]
      when(mockCommonAo.getCurrentTime) thenReturn  DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss").parseDateTime("2018-08-10 00:00:00")
      when(mockCommonAo.EXP_INTERVAL) thenReturn 1
      when(mockCommonAo.SECRET_KEY) thenReturn "secret key"

      val mockSessionDao = mock[SessionDao]
      val mockUserDao = mock[UserDao]
      when(mockUserDao.findById("testEmail")) thenReturn Future.successful(\/-(User("testUser", "testPassword", "testEmail", "testEmail", Set())))
      val authService = new AuthService(mockUserDao, mockSessionDao, mockCommonAo)

      val sessionExt = Await.result(authService.loginService(SessionR("testEmail", "testPassword")), 1000 millis)
      // key is calculated here https://jwt.io/
      /*
        {
          "uid": "testEmail",
          "exp": "2018-01-10T00:01:00.000+11:00",
          "prm": []
        }
       */
      sessionExt.getOrElse("") mustBe SessionExt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTAxLTEwVDAwOjAxOjAwLjAwMCsxMTowMCIsInBybSI6W119.YpKLkDNzxDqZhwlLhrYC4RU_imHwPKRHfOwh3B4OTIM")
    }
  }

  "A renewService" must {
    "return valid session object" in {
      /*
      {
        "uid": "testEmail",
        "exp": "2018-08-06T20:22:54.092+10:00",
        "prm": []
      }
       */
      val sessionExtOld = SessionExt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTA4LTA2VDIwOjIyOjU0LjA5MisxMDowMCIsInBybSI6W119.M9A642hPNSxZSg6COi67kxtO6kGu0ukt6A3YBXMoYDU")

      val mockCommonAo = mock[CommonAo]
      when(mockCommonAo.getCurrentTime) thenReturn  DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss").parseDateTime("2018-08-10 00:00:00")
      when(mockCommonAo.EXP_INTERVAL) thenReturn 1
      when(mockCommonAo.SECRET_KEY) thenReturn "secret key"

      val mockSessionDao = mock[SessionDao]
      when(mockSessionDao.deleteSession(sessionExtOld)) thenReturn Future.successful(\/-(()))

      val mockUserDao = mock[UserDao]
      val authService = new AuthService(mockUserDao, mockSessionDao, mockCommonAo)

      val sessionExt = Await.result(authService.renewService(sessionExtOld), 1000 millis)
      // key is calculated here https://jwt.io/
      /*
       {
          "uid": "testEmail",
          "exp": "2018-01-10T00:01:00.000+11:00",
          "prm": []
        }
       */
      sessionExt.getOrElse("") mustBe SessionExt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTAxLTEwVDAwOjAxOjAwLjAwMCsxMTowMCIsInBybSI6W119.YpKLkDNzxDqZhwlLhrYC4RU_imHwPKRHfOwh3B4OTIM")
    }
  }

  "A checkSession" must {
    "identify correct token" in {
      /*
       {
          "uid": "testEmail",
          "exp": "2018-01-10T00:02:00.000+11:00",
          "prm": []
       }
       */
      val sessionExtOld = SessionExt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTAxLTEwVDAwOjAyOjAwLjAwMCsxMTowMCIsInBybSI6W119.Ty2XSVmV-zLZWcogm38Vck1cmKK0IAA5ZoG7Y4zcVko")

      val mockCommonAo = mock[CommonAo]
      when(mockCommonAo.getCurrentTime) thenReturn  DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss").parseDateTime("2018-08-10 00:00:00")
      when(mockCommonAo.EXP_INTERVAL) thenReturn 2
      when(mockCommonAo.SECRET_KEY) thenReturn "secret key"

      val mockSessionDao = mock[SessionDao]
      when(mockSessionDao.checkSessionInBlackList(sessionExtOld)) thenReturn Future.successful(\/-(()))

      val mockUserDao = mock[UserDao]
      val authService = new AuthService(mockUserDao, mockSessionDao, mockCommonAo)

      val sessionExt = Await.result(authService.checkSession(sessionExtOld), 1000 millis)
      sessionExt.getOrElse("") mustBe ()
    }
  }


}


