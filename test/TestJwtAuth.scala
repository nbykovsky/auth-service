import java.util.concurrent.Executors

import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import akka.stream.Materializer
import controllers.Auth
import models.session.{CommonAo, SessionDao, SessionExt}
import models.user.UserDao
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.when
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.{ExecutionContext, Future}
import org.scalatestplus.play._
import play.api.http.MediaRange.parse
import play.api.http.Status.UNAUTHORIZED
import scalaz.\/-
import services.AuthService
import utils.actionUtils.JwtAuth
import utils.jwtUtils


class TestJwtAuth extends PlaySpec with GuiceOneAppPerSuite  with Results with MockitoSugar  {

  implicit lazy val materializer: Materializer = app.materializer
  implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1))

  "JwtAuth action" should {
    "allow requests if token is valid" in {

      /*
       {
          "uid": "testEmail",
          "exp": "2018-01-10T00:02:00.000+11:00",
          "prm": []
       }
       */
      // this token should be valid
      val token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTAxLTEwVDAwOjAyOjAwLjAwMCsxMTowMCIsInBybSI6W119.Ty2XSVmV-zLZWcogm38Vck1cmKK0IAA5ZoG7Y4zcVko"
      val sessionExtOld = SessionExt(token)

      implicit val mockCommonAo: CommonAo = mock[CommonAo]
      // after parsing this date becomes 2018-01-10T00:00:00.000+11:00
      when(mockCommonAo.getCurrentTime) thenReturn  DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss").parseDateTime("2018-08-10 00:00:00")
      when(mockCommonAo.EXP_INTERVAL) thenReturn 1
      when(mockCommonAo.SECRET_KEY) thenReturn "secret key"

      implicit val authService: AuthService = mock[AuthService]

      when(authService.checkSession(sessionExtOld)) thenReturn Future.successful(\/-(()))

      implicit val mockCC: ControllerComponents = mock[ControllerComponents]

      val jwtAuth = new JwtAuth(new BodyParsers.Default())(ec, mockCC, authService, mockCommonAo)

      val action: EssentialAction = jwtAuth { request =>
        val value = (request.body.asJson.get \ "field").as[String]
        Ok(value)
      }

      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "field": "value" }""")).withHeaders(("Authorization", token))

      val result = call(action, request)

      status(result) mustEqual OK
      contentAsString(result) mustEqual "value"
    }

    "block requests if token doesn't exists" in {
      /*
       {
          "uid": "testEmail",
          "exp": "2018-01-10T00:02:00.000+11:00",
          "prm": []
       }
       */
      val token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJ0ZXN0RW1haWwiLCJleHAiOiIyMDE4LTAxLTEwVDAwOjAyOjAwLjAwMCsxMTowMCIsInBybSI6W119.Ty2XSVmV-zLZWcogm38Vck1cmKK0IAA5ZoG7Y4zcVko"
      val sessionExtOld = SessionExt(token)

      implicit val mockCommonAo: CommonAo = mock[CommonAo]
      when(mockCommonAo.getCurrentTime) thenReturn  DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss").parseDateTime("2018-08-10 00:00:00")
      when(mockCommonAo.EXP_INTERVAL) thenReturn 1
      when(mockCommonAo.SECRET_KEY) thenReturn "secret key"

      implicit val authService: AuthService = mock[AuthService]

      when(authService.checkSession(sessionExtOld)) thenReturn Future.successful(\/-(()))

      implicit val mockCC: ControllerComponents = mock[ControllerComponents]

      val jwtAuth = new JwtAuth(new BodyParsers.Default())(ec, mockCC, authService, mockCommonAo)

      val action: EssentialAction = jwtAuth { request =>
        val value = (request.body.asJson.get \ "field").as[String]
        Ok(value)
      }

      // request doesn't have Authorization header
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "field": "value" }"""))

      val result = call(action, request)

      status(result) mustEqual UNAUTHORIZED
      contentAsString(result) mustEqual "Token doesn't exists"

    }

  }
}
