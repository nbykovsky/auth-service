
# Get details of all users

All users related information

**URL** : `/user/`

**Method** : `GET`

**Headers**

```
Authorization: <jwt token>
```

**Auth required** : YES


## Success Response

**Condition** : When token is valid, not expired and not invoked

**Code** : `200 OK`

**Content example**

```json
{
	"ok": [
		{
			"name": "First User1",
			"password": "password",
			"email": "first@user1",
			"userId": "email",
			"permissions": []
		}
	]
}
```

## Error Response

**Condition** : Token expired or non valid or authorization header doesn't exists.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
	"error": "Token expired"
}
```
