
# Create user

Creates the new user.

**URL** : `/user`

**Method** : `POST`

**Headers**

```
Content-Type: application/json
```

**Auth required** : NO

**Data constraints**

```json
{
	"name": "<name>",
	"email": "<email>",
	"password": "<password>"
}
```

**Data example**

```json
{
	"name": "First User",
	"email": "first@user",
	"password": "first password"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	"ok": "user created"
}
```

## Error Response

**Condition** : If user already exists.

**Code** : `409 CONFLICT`

**Content** :

```json
{
	"error": "User exists"
}
```
