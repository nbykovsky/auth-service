
# Login

Exchanges login/password to JWT token.

**URL** : `/login/`

**Method** : `POST`

**Headers**

```
Content-Type: application/json
```

**Auth required** : NO

**Data constraints**

```json
{
    "userId": "<user's email>",
    "password": "<user's password>"
}
```

**Data example**

```json
{
	"userId": "test@email.com",
	"password": "password@1234"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "ok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJlbWFpbCIsImV4cCI6IjIwMTgtMDgtMDdUMjA6NDk6MzkuNzUzKzEwOjAwIiwicHJtIjpbXX0.0bCEbOLIoXOs2tW4wGw6R1yp1XWvPCwa0mIgugQn0oM"
}
```

## Error Response

**Condition** : If user with 'userId' does not exists.

**Code** : `404 NOT FOUND`

**Content** :

```json
{
	"error": "User doesn't exists"
}
```

**Condition** : If user with 'userId' exists but password is wrong.

**Code** : `401 UNAUTORIZED`

**Content** :

```json
{
	"error": "Password is wrong"
}
```