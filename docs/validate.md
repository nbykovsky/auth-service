
# Validate token

Endpoint checks whether JWT token is valid.

**URL** : `/check`

**Method** : `GET`

**Headers**

```
Authorization: <jwt token>
```

**Auth required** : YES


## Success Response

**Condition** : When token is valid, not expired and not invoked.

**Code** : `200 OK`

**Content example**

```json
{
	"ok": "Token is valid"
}
```

## Error Response

**Condition** : Token expired or non valid or authorization header doesn't exists.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
	"error": "Token expired"
}
```
