
# Invoke token

Makes jwt token non valid

**URL** : `/login`

**Method** : `DELETE`

**Headers**

```
Authorization: <jwt token>
```

**Auth required** : YES


## Success Response

**Condition** : When token is valid, not expired and not invoked.

**Code** : `200 OK`

**Content example**

```json
{
	"ok": "User was logged out"
}
```

## Error Response

**Condition** : Token expired or non valid or authorization header doesn't exists.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
	"error": "Token expired"
}
```
