
# Update user

Updated an existing user

**URL** : `/user/<userId>`

**Method** : `PUT`

**Headers**

```
Content-Type: application/json
Authorization: <jwt token>
```

**Auth required** : YES

**Data constraints**

```json
{
	"name": "<name (optional)>",
	"email": "<email (optional)>",
	"password": "<password (optional)>"
}
```

**Data example**

```json
{
	"name": "Second User",
	"email": "second@user",
	"password": "second password"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	"ok": "User updated"
}
```

## Error Response

**Condition** : Token expired or non valid or authorization header doesn't exists.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
	"error": "Token expired"
}
```
