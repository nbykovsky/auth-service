
# Renew token

Changes one valid token to another. The first token will be invoked

**URL** : `/renew`

**Method** : `GET`

**Headers**

```
Authorization: <jwt token>
```

**Auth required** : YES


## Success Response

**Condition** : When token is valid, not expired and not invoked.

**Code** : `200 OK`

**Content example**

```json
{
	"ok": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJlbWFpbCIsImV4cCI6IjIwMTgtMDgtMDhUMTk6Mjc6NDguOTI1KzEwOjAwIiwicHJtIjpbXX0.ei9Ru-tTEU8jicjaAS4Uq3ciX3_Yy7qzVL0nWysYv_k"
}
```

## Error Response

**Condition** : Token expired or non valid or authorization header doesn't exists.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
	"error": "Token expired"
}
```
